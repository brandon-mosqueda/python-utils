import re

ALLOWED_SPECIAL_LETTERS = {    
    'á': 'a',
    'Á': 'A',
    'ã': 'a',
    'Ã': 'A',

    'è': 'e',
    'é': 'e',
    'É': 'E',
    
    'í': 'i',
    'Í': 'I',
    
    'ó': 'o',
    'Ó': 'O',
    'ö': 'o',
    'ò': 'o',
    'Ö': 'O',
    'Ò': 'O',
    
    'ú': 'u',
    'Ú': 'U',
    
    'ç': 'c',
    'Ç': 'C',
    
    'ñ': 'n',
    'Ñ': 'N'
}

def is_number(char):
    code = ord(char)

    return code >= 48 and code <= 57

def is_special_letter(letter):
    try:
        ALLOWED_SPECIAL_LETTERS[letter]
        return True
    except KeyError:
        return False

def is_letter_or_special_letter(char):
    return is_letter(char, True)

def is_letter(char, allow_special_letters=False):
    if not isinstance(char, str) or len(char) > 1:
        return False

    code = ord(char)
    
    return (
        (code >= 97 and code <= 122) # Upper
        or (code >= 65 and code <= 90) # Lower
        or (allow_special_letters and is_special_letter(char))
    )

def get_correct_letter_from_special_letter(letter):
    if not is_special_letter(letter):
        return ''

    return ALLOWED_SPECIAL_LETTERS[letter]

def trim_right(text, char=' '):
    regex = re.compile(char + '+$')

    return re.sub(regex, '', text)

def trim_left(text, char=' '):
    regex = re.compile('^' + char + '+')

    return re.sub(regex, '', text)

def trim(text, char=' '):
    return trim_left(trim_right(text, char), char)