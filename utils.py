import os
import re
import time
import glob
import json

from alphanumeric import trim, is_number, is_special_letter, is_letter, get_correct_letter_from_special_letter

def clear_screen(): 
    if(os.name == 'posix'):
        _ = os.system('clear') 
    else:
        _ = os.system('cls')

def get_elapsed_time(start_time):
    return round(time.time() - start_time, 2)

def write_json(path, data):
    with open(path, 'w+') as json_writer:
        json.dump(data, json_writer)

def read_json(path):
    with open(path) as json_writer:
        return json.load(json_writer)

def get_content_list_of_dir(dir_path, include_dirs=False, glob_str='*.*'):
    if (include_dirs):
        glob_str = '*'
        
    return glob.glob(os.path.join(dir_path, glob_str))

def replace_all_break_lines(text, char_by):
    return text.replace('\n', char_by).replace('\r', '')

def print_inline(text):
    print(text, end="", flush=True),

def get_first_by_regex(regex, text):
    if (text is None):
        return None

    try:
        return re.findall(regex, text)[0]
    except IndexError:
        return None

def get_file_name(file_path, keep_extesion=False):
    base = os.path.basename(file_path)

    if (keep_extesion):
        return base

    return os.path.splitext(base)[0]

def none_if_empty(value):
    if (not isinstance(value, str)):
        return value

    if (not value.strip()):
        return None

    return value.strip()

def delete_two_or_more_consecutive_middle_scripts(text):
    return re.sub(r'--+', '-', text)

def standardize(text, allow_numbers=False):
    text = text.strip().lower()
    new_text = ''

    for char in text:
        if is_letter(char):
            new_text += char
        elif is_special_letter(char):
            new_text += get_correct_letter_from_special_letter(char)
        elif char is ' ':
            new_text += '-'
        elif allow_numbers and is_number(char):
            new_text += char
    
    return trim(delete_two_or_more_consecutive_middle_scripts(new_text), '-');