# Requires mysql connector 8
import mysql.connector

class MySQLWrapper:
    _DEFAULT_PORT = 3306

    def __init__(self, credentials):
        port = self._get_port(credentials)

        self._connector = mysql.connector.connect(
            host=credentials['host'],
            database=credentials['database'],
            user=credentials['user'],
            passwd=credentials['password'],
            port=port
        )

    def _get_port(self, credentials):
        try:
            return credentials['port']
        except KeyError:
            return self._DEFAULT_PORT

    def query(self, sql, params={}):
        cursor = self._connector.cursor(dictionary=True)
        cursor.execute(sql, params)

        return cursor

    def execute_query(self, sql, params={}):
        cursor = self.query(sql, params)
      
        self._connector.commit()

        return cursor

    def get_by_sql(self, sql, params = {}):
        cursor = self.query(sql, params)
      
        return cursor.fetchall()

    def start_transaction(self):
        self.disable_autocommit()

        return self.query('START TRANSACTION')

    def commit_transaction(self):
        return self.query('COMMIT')

    def rollback_transaction(self):
        return self.query('ROLLBACK')