from elasticsearch import Elasticsearch

class ElasticsearchWrapper:
  def __init__(self, doc_type, credentials):
    self.connector = Elasticsearch([ credentials ])
    self.index = credentials[ 'index' ]
    self.doc_type = doc_type
    self.limit_records = 10

  def query(self, content):
    return self.connector.search(
             index = self.index, 
             body = content, 
             size = self.limit_records
           )

  def get_records_by_query(self, content, keep_record_metadata = False):
    data = self.query(content)[ 'hits' ][ 'hits' ]

    if(keep_record_metadata):
      return data

    new_data = []

    for record in data:
      new_data.append(record[ '_source' ])

    return new_data

  def get_all(self):
    return self.get_records_by_query({})

  def insert(self, data):
    result = self.connector.index(
               index = self.index, 
               doc_type = self.doc_type, 
               body = data
             )
    
    return result[ 'result' ] == 'created'

  def delete_by_id(self, id):
    result = self.connector.delete(
               index = self.index,
               doc_type = self.doc_type, 
               id = id
             )

    return result[ 'deleted' ] == 'deleted'